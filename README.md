# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###


* Quick summary


* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
1. Clone repo
2. Start API
3. Open any API Development Environment for example [Postman][Postman]:

* [/user/anyId][getOrDeletesUserById] Get User by ID
    1. Gets the user from the Database (H2) by it's ID with Http Status GET.
    2. If ID is not in the database returns Status: 404 NOT_FOUND and message "ERROR : There was an error either accessing or during db transaction.".
    3. If ID is not a number returns Status: 404 NOT_FOUND and same message as before.
    4. If ID is number and letters it returns Status: 400 BAD_REQUEST.
* [/user][addsUserById] Adds User by JSON
    1. Adds a user via JSON(SNAKE_CASE) using Http Status POST.
    2. If ID is not present in the JSON it's automatically generated.
    3. If ID is present in the JSON updates corresponding record or adds a new one.
    4. If JSON is not properly, returns Status: 404 NOT_FOUND and same message as point 2 of last endpoint.
    5. JSON properly written example:
    {
        "id": "1",
        "sfg": "Martin",
        "last_name": "Fabra",
        "email": "somemail@mail.com",
        "loans": []
    }
* [/user/anyId][getOrDeletesUserById]  Deletes User by ID
    1. Deletes the user from the Database by it's ID with Http Status DELETE
    2. If ID is not in the database returns Status: 404 NOT_FOUND and message "ERROR : There was an error either accessing or during db transaction.".
    3. If ID is not a number returns Status: 400 BAD_REQUEST
* [/loans][getLoans] Gets loans paginated
    1. Gets the loans from the Database paginated remember page starts at 0
    2. Example of Usage link: http://localhost:8080/loans?page=0&size=50
    
        Should get this result:
        "{
             "content": [
                 {
                     "id": 1,
                     "user_id": 1,
                     "total": 25000
                 },
                 {
                     "id": 2,
                     "user_id": 1,
                     "total": 5000
                 },
                 {
                     "id": 4,
                     "user_id": 2,
                     "total": 95000
                 },
                 {
                     "id": 5,
                     "user_id": 2,
                     "total": 95000
                 },
                 {
                     "id": 6,
                     "user_id": 4,
                     "total": 86000
                 },
                 {
                     "id": 7,
                     "user_id": 4,
                     "total": 4000
                 },
                 {
                     "id": 8,
                     "user_id": 5,
                     "total": 100000
                 },
                 {
                     "id": 9,
                     "user_id": 5,
                     "total": 95000
                 },
                 {
                     "id": 10,
                     "user_id": 6,
                     "total": 50
                 }
             ],
             "pageable": {
                 "sort": {
                     "sorted": false,
                     "unsorted": true,
                     "empty": true
                 },
                 "offset": 0,
                 "page_number": 0,
                 "page_size": 50,
                 "unpaged": false,
                 "paged": true
             },
             "last": true,
             "total_elements": 9,
             "total_pages": 1,
             "size": 50,
             "number": 0,
             "sort": {
                 "sorted": false,
                 "unsorted": true,
                 "empty": true
             },
             "number_of_elements": 9,
             "first": true,
             "empty": false
         }"
* [/loans?user_id=anyId][getLoansWithUserId]  Gets loans paginated filtered by user_id.
    1. If user_id is not in the database it returns empty content with 200 ok Status.
    2. If user_id is letters or letters and numbers it returns 400 BAD_REQUEST status.       
        

* Configuration

All the Configuration properties are on src\main\resources\application.properties

* Dependencies

[ Caffeine Cache](https://github.com/ben-manes/caffeine)

[ Spring boot ](https://spring.io/projects/spring-boot)

[ H2 Database ](http://www.h2database.com/html/main.html)

* How to run tests
    
    1. Open git bash
    2. mvn clean install


### Who do I talk to? ###

* Repo owner or admin

calixto.y.gonzalez@gmail.com

[Postman]: https://www.getpostman.com/ "Postman"
[getOrDeletesUserById]: http://localhost:8080/users/1  "Get or Deletes user by Id (int this example gets user with id 1)"
[addsUserById]: http://localhost:8080/users  "Adds user by Id (int this example gets user with id 1)"
[getLoans]: http://localhost:8080/loans "Gets loans paginated" 
[getLoansWithUserId]: http://localhost:8080/loans?page=0&size=50&user_id=4 "Gets loan paginated and filter by user_id 4"