package com.cash.online.poc.controller;

import com.cash.online.poc.entity.User;
import com.cash.online.poc.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserController
 */
@RequestMapping(value = "/users")
@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    private UserService userService;

    /**
     * UserController constructor.
     *
     * @param userService {@link UserService}
     */
    @Autowired
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    /**
     * Gets {@link User} by id, paginated.
     *
     * @param id long
     * @return responseEntity {@link ResponseEntity}
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getUserById(@PathVariable("id") final long id) {
        User user = userService.findUserById(id);
        logger.info("USER :" + user.toString() + " successfully obtained.");
        return ResponseEntity.ok(user);
    }

    /**
     * Gets the {@link User} from JSON.
     * Adds {@link User}, gets {@link User} after it was successfully inserted.
     *
     * @param user {@link User}
     * @return ResponseEntity
     */
    @PostMapping
    public ResponseEntity addUser(@RequestBody final User user) {
        User userAfterInsertion = userService.addUser(user);
        logger.info("USER :" + userAfterInsertion.toString() + " added successfully.");
        return ResponseEntity.ok(userAfterInsertion);
    }

    /**
     * Deletes {@link User} by id.
     *
     * @param id long
     * @return responseEntity {@link ResponseEntity}
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteUserById(@PathVariable("id") final long id) {
        userService.deleteUserById(id);
        logger.info("USER deleted successfully.");
        return ResponseEntity.ok().build();
    }

}


