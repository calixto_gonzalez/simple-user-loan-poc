package com.cash.online.poc.controller;

import com.cash.online.poc.entity.Loan;
import com.cash.online.poc.service.LoanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * LoanController
 */
@RestController
@RequestMapping(value = "/loans")
public class LoanController {
    private static final Logger logger = LoggerFactory.getLogger(LoanController.class);
    private LoanService loanService;

    /**
     * LoanController Constructor.
     *
     * @param loanService {@link LoanService}
     */
    @Autowired
    public LoanController(final LoanService loanService) {
        this.loanService = loanService;
    }

    /**
     * Gets all the loans paginated.
     *
     * @param pageable {@link Pageable}
     * @return page {@link Page<Loan>}
     */
    @GetMapping
    public ResponseEntity<Page<Loan>> getLoans(final Pageable pageable) {
        Page<Loan> loans = loanService.getLoans(pageable);
        logger.info("Loans where successfully brought.");
        return ResponseEntity.ok(loans);
    }

    /**
     * Gets all the loans paginated with the possibility of filtering by id.
     *
     * @param id       long
     * @param pageable {@link Pageable}
     * @return loanPage {@link Page<Loan>}
     */
    @GetMapping(params = {"user_id"})
    public ResponseEntity<Page<Loan>> getLoansById(@RequestParam("user_id") final long id, final Pageable pageable) {
        Page<Loan> loanPage = loanService.getLoansById(id, pageable);
        logger.info("Loans where successfully brought.");
        return ResponseEntity.ok(loanPage);

    }


}
