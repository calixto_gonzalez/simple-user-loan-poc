package com.cash.online.poc.repository;

import com.cash.online.poc.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * {@link UserRepository} interface with methods to access DB.
 */
public interface UserRepository extends JpaRepository<User, Long> {
}
