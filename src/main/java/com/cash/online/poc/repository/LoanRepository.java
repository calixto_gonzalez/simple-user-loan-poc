package com.cash.online.poc.repository;

import com.cash.online.poc.entity.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/**
 * {@link LoanRepository} interface with methods to access DB.
 */
public interface LoanRepository extends JpaRepository<Loan, Long> {
    /**
     * finds all {@link com.cash.online.poc.entity.User} by id
     *
     * @param id       long
     * @param pageable {@link Pageable}
     * @return page {@link Page<Loan>}
     */
    Page<Loan> findAllById(@Param("user_id") long id, Pageable pageable);
}
