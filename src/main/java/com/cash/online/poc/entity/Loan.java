package com.cash.online.poc.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "loan")
public class Loan {

    public Loan() {
    }

    @Id
    private long id;

    private long userId;

    private BigDecimal total;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Loan{");
        sb.append("id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", total=").append(total);
        sb.append('}');
        return sb.toString();
    }
}
