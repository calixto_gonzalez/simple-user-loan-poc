package com.cash.online.poc.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

/**
 * General Exception handler.
 */
@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

     private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    /**
     * Exception Hanlder for al DB errors, encapsulation was done in order to protect valuable information.
     *
     * @param e          {@link RuntimeException}
     * @param webRequest {@link WebRequest}
     * @return responseEntity {@link ResponseEntity}
     */
    @ExceptionHandler({EmptyResultDataAccessException.class, DataAccessException.class, DataAccessResourceFailureException.class, NoSuchElementException.class})
    public ResponseEntity daoExceptionHandler(final RuntimeException e, final WebRequest webRequest) {
        String error = "ERROR : There was an error either accessing or during db transaction.";
        logger.error(error, e);
        return handleExceptionInternal(e, error, new HttpHeaders(), HttpStatus.NOT_FOUND, webRequest);
    }
}
