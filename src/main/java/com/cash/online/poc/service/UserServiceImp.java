package com.cash.online.poc.service;

import com.cash.online.poc.entity.User;
import com.cash.online.poc.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
/**
 * Implementacion of {@link UserService}
 */
@Service
@Transactional
public class UserServiceImp implements UserService {

    private final UserRepository userRepository;

    /**
     *
     * @param userRepository {@link UserRepository}
     */
    @Autowired
    public UserServiceImp(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    @Cacheable(value = "servicesCache", key = "#id")
    public User findUserById(final long id) {
        return userRepository.findById(id).get();
    }

    @Override
    public User addUser(final User user) {
        return userRepository.save(user);
    }

    @Override
    public void deleteUserById(final long id) {
        userRepository.deleteById(id);
    }
}
