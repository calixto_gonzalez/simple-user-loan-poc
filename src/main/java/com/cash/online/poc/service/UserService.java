package com.cash.online.poc.service;

import com.cash.online.poc.entity.User;
/**
 * interface for Service
 */
public interface UserService {
    /**
     * Find {@link User} by id.
     *
     * @param id long
     * @return user {@link User}
     */
    User findUserById(long id);

    /**
     * Adds {@link User} by id.
     *
     * @param user {@link User}
     * @return user {@link User}
     */
    User addUser(User user);

    /**
     * Deletes {@link User} by id.
     *
     * @param id long
     */
    void deleteUserById(long id);
}
