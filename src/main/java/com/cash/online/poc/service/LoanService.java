package com.cash.online.poc.service;

import com.cash.online.poc.entity.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * interface for Service
 */
public interface LoanService {
    /**
     * Gets all {@link Loan}.
     *
     * @param pageable {@link Pageable}
     * @return page {@link Page<Loan>}
     */
    Page<Loan> getLoans(Pageable pageable);

    /**
     * Gets {@link Loan} by id paginated.
     *
     * @param id       long
     * @param pageable {@link Pageable}
     * @return page {@link Page<Loan>}
     */
    Page<Loan> getLoansById(long id, Pageable pageable);
}
