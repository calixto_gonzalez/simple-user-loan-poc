package com.cash.online.poc.service;

import com.cash.online.poc.entity.Loan;
import com.cash.online.poc.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Implementacion of {@link LoanService}
 */
@Service
@Transactional
public class LoanServiceImp implements LoanService {
    private final LoanRepository loanRepository;

    /**
     * {@link LoanServiceImp} constructor.
     *
     * @param loanRepository {@link LoanRepository}
     */
    @Autowired
    public LoanServiceImp(final LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    @Override
    @Cacheable(value = "servicesCache")
    public Page<Loan> getLoans(final Pageable pageable) {
        return loanRepository.findAll(pageable);
    }

    @Override
    @Cacheable(value = "servicesCache")
    public Page<Loan> getLoansById(final long id, final Pageable pageable) {
        return loanRepository.findAllById(id, pageable);
    }
}
