INSERT INTO USER(ID,EMAIL,NAME,LASTNAME) VALUES
(1,'somemail@mail.com','Darío','Benedetto'),
(2,'somemail@mail.com','Cristian','Pavón'),
(3,'somemail@mail.com','Paolo','Goltz'),
(4,'somemail@mail.com','Frank', 'Fabra'),
(5,'somemail@mail.com','Pablo','Pérez'),
(6,'somemail@mail.com','Edwin Andres','Cardona');

INSERT INTO LOAN(ID,USER_ID,TOTAL) VALUES
(1,1,25000),
(2,1,5000),
(4,2,95000),
(5,2,95000),
(6,4,86000),
(7,4,4000),
(8,5,100000),
(9,5,95000),
(10,6,50);
