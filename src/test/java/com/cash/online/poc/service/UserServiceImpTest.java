package com.cash.online.poc.service;

import com.cash.online.poc.entity.User;
import com.cash.online.poc.repository.UserRepository;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;

public class UserServiceImpTest {

    public static final int ID_NOT_IN_DB = 999;
    private UserRepository userRepository;
    private UserService userService;

    @Before
    public void setUp() {
        userRepository = mock(UserRepository.class);
        userService = new UserServiceImp(userRepository);
    }

    @Test
    public void findUserByIdShouldFindUser() {
        Mockito.doReturn(Optional.of(getUserForTest())).when(userRepository).findById(anyLong());
        User userAfterExecution = userService.findUserById(5);
        assertNotNull(userAfterExecution);
        assertEquals("Alberto",userAfterExecution.getFirstName());
        assertEquals("Gomez",userAfterExecution.getLastName());
        assertEquals("some@mail.com",userAfterExecution.getEmail());
        assertEquals(5,userAfterExecution.getId());
    }

    @Test(expected = NoSuchElementException.class)
    public void findUserByIdShouldThrowExceptionIfIdIsNotInDb() {
        Mockito.doThrow(NoSuchElementException.class).when(userRepository).findById(anyLong());
        userService.findUserById(ID_NOT_IN_DB);
    }

    //Ignored Mockito having trouble mocking the return because of Optional object
    @Test
    @Ignore
    public void addUserShouldAddUserAndReturnIt() {
        Optional optional = Optional.of(getUserForTest());
        Mockito.doReturn(optional).when(userRepository).save(any(User.class));
        User userAfterExecution = userService.addUser(getUserForTest());
        assertNotNull(userAfterExecution);
        assertEquals("Alberto",userAfterExecution.getFirstName());
        assertEquals("Gomez",userAfterExecution.getLastName());
        assertEquals("some@mail.com",userAfterExecution.getEmail());
        assertEquals(5,userAfterExecution.getId());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void addUserShouldThrowExceptionWithNullNameOrLastName() {
        User user = getUserForTest();
        user.setFirstName(null);
        Mockito.doThrow(DataIntegrityViolationException.class).when(userRepository).save(user);
        userService.addUser(user);
    }


    @Test
    public void deleteUserByIdShouldDeleteUserById() {
        Mockito.doNothing().when(userRepository).delete(getUserForTest());
        userService.deleteUserById(1);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void deleteUserByIdShouldThrowExceptionIfIdIsNotInDb() {
        Mockito.doThrow(EmptyResultDataAccessException.class).when(userRepository).deleteById(anyLong());
        userService.deleteUserById(ID_NOT_IN_DB);
    }

    private User getUserForTest() {
        User user = new User();
        user.setEmail("some@mail.com");
        user.setFirstName("Alberto");
        user.setLastName("Gomez");
        user.setId(5);
        user.setLoans(Collections.emptyList());
        return user;
    }
}