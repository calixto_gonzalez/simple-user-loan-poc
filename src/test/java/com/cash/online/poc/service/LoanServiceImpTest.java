package com.cash.online.poc.service;

import com.cash.online.poc.entity.Loan;
import com.cash.online.poc.repository.LoanRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

public class LoanServiceImpTest {


    public static final int ID_NOT_IN_DB = 999;
    private LoanRepository loanRepository;
    private LoanService loanService;

    @Before
    public void setUp() throws Exception {
        loanRepository = Mockito.mock(LoanRepository.class);
        loanService = new LoanServiceImp(loanRepository);
    }

    @Test
    public void getLoansShouldGetLoans() {
        Mockito.doReturn(getPageForTest()).when(loanRepository).findAll(any(Pageable.class));
        Page<Loan> loansPage = loanService.getLoans(getPageForTest().getPageable());

        assertEquals(0,loansPage.getTotalPages());
        assertEquals(1,loansPage.getSize());
        assertEquals(1,loansPage.getNumberOfElements());
        assertEquals(1,loansPage.getContent().get(0).getId());
        assertEquals(BigDecimal.valueOf(69000),loansPage.getContent().get(0).getTotal());
        assertEquals(5,loansPage.getContent().get(0).getUserId());
    }

    @Test
    public void getLoansByIdShouldGetLoanById() {
        Mockito.doReturn(getPageForTest()).when(loanRepository).findAllById(anyLong(),any(Pageable.class));
        Page<Loan> loansPage = loanService.getLoansById(1,getPageForTest().getPageable());

        assertEquals(1,loansPage.getContent().get(0).getId());
        assertEquals(BigDecimal.valueOf(69000),loansPage.getContent().get(0).getTotal());
        assertEquals(5,loansPage.getContent().get(0).getUserId());

    }

    private Loan getLoanForTest() {
        Loan loan = new Loan();
        loan.setId(1);
        loan.setTotal(BigDecimal.valueOf(69000));
        loan.setUserId(5);
        return loan;
    }
    private Page getPageForTest() {
        Page<Loan> loans = new Page<Loan>() {
            @Override
            public int getTotalPages() {
                return 0;
            }

            @Override
            public long getTotalElements() {
                return 1;
            }

            @Override
            public <U> Page<U> map(Function<? super Loan, ? extends U> function) {
                return null;
            }

            @Override
            public int getNumber() {
                return 0;
            }

            @Override
            public int getSize() {
                return 1;
            }

            @Override
            public int getNumberOfElements() {
                return 1;
            }

            @Override
            public List<Loan> getContent() {
                List<Loan> listOfLoans = new ArrayList<>();
                listOfLoans.add(getLoanForTest());
                return listOfLoans;
            }

            @Override
            public boolean hasContent() {
                return true;
            }

            @Override
            public Sort getSort() {
                return null;
            }

            @Override
            public boolean isFirst() {
                return false;
            }

            @Override
            public boolean isLast() {
                return false;
            }

            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }

            @Override
            public Pageable nextPageable() {
                return null;
            }

            @Override
            public Pageable previousPageable() {
                return null;
            }

            @Override
            public Iterator<Loan> iterator() {
                return null;
            }
        };
        loans.getContent().add(getLoanForTest());
        return loans;
    }
}