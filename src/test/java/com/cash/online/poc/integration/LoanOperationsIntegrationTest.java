package com.cash.online.poc.integration;


import com.cash.online.poc.DemoApplication;
import com.cash.online.poc.entity.Loan;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LoanOperationsIntegrationTest {

    @LocalServerPort
    private int port;


    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Before
    public void setup() {

    }


    @Test
    public void getLoansShouldThrowExceptionIfLoanIsNotInDataBase() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity response = restTemplate.exchange(createURLWithPort("/loans/99"), HttpMethod.GET, entity, Loan.class);
        assertEquals(HttpStatus.NOT_FOUND,response.getStatusCode());
    }

    public <T> Page<T> exchangeAsPage(String uri, ParameterizedTypeReference<Page<T>> responseType) {
        return restTemplate.exchange(uri, HttpMethod.GET, null, responseType).getBody();
    }

    private String createURLWithPort(String uri) {
        return  "http://localhost:" + port + uri;
    }


}
