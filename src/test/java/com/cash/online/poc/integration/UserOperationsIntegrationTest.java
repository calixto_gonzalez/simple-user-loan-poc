package com.cash.online.poc.integration;

import com.cash.online.poc.DemoApplication;
import com.cash.online.poc.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserOperationsIntegrationTest {
    @LocalServerPort
    private int port;


    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Before
    public void setup() {

        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("{\n" +
                "\t\t\"id\" : \"1\",\n" +
                "        \"first_name\": \"Martin\",\n" +
                "        \"last_name\": \"Fabra\",\n" +
                "        \"email\": \"somemail@mail.com\",\n" +
                "        \"loans\": []\n" +
                "}", headers);
        ResponseEntity prueba = restTemplate.exchange(createURLWithPort("/users"), HttpMethod.POST, entity, User.class);

    }


    @Test
    public void getUserFromDataBaseShouldSaveUser() throws IOException {

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity response = restTemplate.exchange(createURLWithPort("/users/1"), HttpMethod.GET, entity, User.class);
        User user = (User) response.getBody();
        assertNotNull(response.getBody());
        assertEquals(1,user.getId());
        assertEquals("Martin",user.getFirstName());
        assertEquals("Fabra",user.getLastName());
        assertEquals("somemail@mail.com",user.getEmail());
        assertEquals(Collections.emptyList(),user.getLoans());
        assertEquals(HttpStatus.OK,response.getStatusCode());

    }

    @Test
    public void getUserFromDataBaseShouldThrowExceptionIfUserIsNotValid() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity response = restTemplate.exchange(createURLWithPort("/users/sarasa"), HttpMethod.GET, entity, User.class);
        assertEquals(HttpStatus.BAD_REQUEST,response.getStatusCode());
    }

    @Test
    public void deleteUserFromDataBaseShouldDeleteUser() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity response = restTemplate.exchange(createURLWithPort("/users/1"), HttpMethod.DELETE, entity, User.class);
        assertEquals(HttpStatus.OK,response.getStatusCode());

    }



    private String createURLWithPort(String uri) {
        return  "http://localhost:" + port + uri;
    }

    private User getUserForTest() {
        User user = new User();
        user.setEmail("some@mail.com");
        user.setFirstName("Alberto");
        user.setLastName("Gomez");
        user.setId(5);
        user.setLoans(Collections.emptyList());
        return user;
    }

}
