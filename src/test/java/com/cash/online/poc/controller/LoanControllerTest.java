package com.cash.online.poc.controller;

import com.cash.online.poc.entity.Loan;
import com.cash.online.poc.service.LoanService;
import com.cash.online.poc.service.LoanServiceImp;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class LoanControllerTest {
    @Autowired
    MockMvc mockMvc;

    public static final int FIRST_ID_0 = 0;
    private LoanService loanService;
    private LoanController loanController;

    @Before
    public void setUp() throws Exception {
        loanService = mock(LoanServiceImp.class);
        loanController = new LoanController(loanService);
        mockMvc = MockMvcBuilders.standaloneSetup(loanController).build();

    }

    @Test
    public void getLoansShouldGetLoans() {
        Mockito.doReturn(getPageForTest()).when(loanService).getLoans(any());
        Page<Loan> loanAfterExecution = loanController.getLoans(getPageForTest().getPageable()).getBody();
        Loan loan = loanAfterExecution.getContent().get(FIRST_ID_0);
        assertEquals(0,loanAfterExecution.getTotalPages());
        assertEquals(1,loanAfterExecution.getSize());
        assertEquals(1,loanAfterExecution.getNumberOfElements());
        assertEquals(1,loan.getId());
        assertEquals(BigDecimal.valueOf(69000),loan.getTotal());
        assertEquals(5,loan.getUserId());
    }

    @Test
    public void getLoansById() {
        Mockito.doReturn(getPageForTest()).when(loanService).getLoansById(anyLong(),any(Pageable.class));
        Page<Loan> loanAfterExecution = loanController.getLoansById(1,getPageForTest().getPageable()).getBody();
        Loan loan = loanAfterExecution.getContent().get(FIRST_ID_0);
        assertEquals(1,loan.getId());
        assertEquals(BigDecimal.valueOf(69000),loan.getTotal());
        assertEquals(5,loan.getUserId());
    }

    @Test
    public void getLoansByIdShouldThrowNotFoundIfIdIsNotNumber() throws Exception {
        mockMvc.perform(get("/loans/{user_id}", "sarasa")).andExpect(status().isNotFound());
    }

    private Loan getLoanForTest() {
        Loan loan = new Loan();
        loan.setId(1);
        loan.setTotal(BigDecimal.valueOf(69000));
        loan.setUserId(5);
        return loan;
    }

    private Page getPageForTest() {
       Page<Loan> loans = new Page<Loan>() {
           @Override
           public int getTotalPages() {
               return FIRST_ID_0;
           }

           @Override
           public long getTotalElements() {
               return 1;
           }

           @Override
           public <U> Page<U> map(Function<? super Loan, ? extends U> function) {
               return null;
           }

           @Override
           public int getNumber() {
               return FIRST_ID_0;
           }

           @Override
           public int getSize() {
               return 1;
           }

           @Override
           public int getNumberOfElements() {
               return 1;
           }

           @Override
           public List<Loan> getContent() {
                List<Loan> listOfLoans = new ArrayList<>();
                listOfLoans.add(getLoanForTest());
               return listOfLoans;
           }

           @Override
           public boolean hasContent() {
               return true;
           }

           @Override
           public Sort getSort() {
               return null;
           }

           @Override
           public boolean isFirst() {
               return false;
           }

           @Override
           public boolean isLast() {
               return false;
           }

           @Override
           public boolean hasNext() {
               return false;
           }

           @Override
           public boolean hasPrevious() {
               return false;
           }

           @Override
           public Pageable nextPageable() {
               return null;
           }

           @Override
           public Pageable previousPageable() {
               return null;
           }

           @Override
           public Iterator<Loan> iterator() {
               return null;
           }
       };
       loans.getContent().add(getLoanForTest());
       return loans;
    }
}