package com.cash.online.poc.controller;

import com.cash.online.poc.entity.User;
import com.cash.online.poc.repository.UserRepository;
import com.cash.online.poc.service.UserService;
import com.cash.online.poc.service.UserServiceImp;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest {

    private static final int BIG_ID_NOT_IN_DB = 100000;

    @Autowired
    private MockMvc mockMvc;

    private UserController userController;
    private UserService userService;

    @Before
    public void setup() {
        userService = Mockito.mock(UserServiceImp.class);
        userController = new UserController(userService);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();

    }

    @Test
    public void getUserByIdShouldGetUser() {
        Mockito.doReturn(getUserForTest()).when(userService).findUserById(5);
        ResponseEntity responseEntity = userController.getUserById(5);
        final User userAfterExecution = (User) responseEntity.getBody();
        assertNotNull(userAfterExecution);
        assertEquals("Alberto",userAfterExecution.getFirstName());
        assertEquals("Gomez",userAfterExecution.getLastName());
        assertEquals("some@mail.com",userAfterExecution.getEmail());
        assertEquals(5,userAfterExecution.getId());
        assertEquals(Collections.emptyList(),userAfterExecution.getLoans());
    }

    @Test(expected = NoSuchElementException.class)
    public void getUserByIdShouldGetUserShouldThrowExceptionIfIdIsNotInDB() {
        Mockito.doThrow(NoSuchElementException.class).when(userService).findUserById(BIG_ID_NOT_IN_DB);
        userController.getUserById(BIG_ID_NOT_IN_DB);
    }

    @Test
    public void getUserByIdShouldThrowExceptionIfIdIsNotNumber() throws Exception{
        mockMvc.perform(get("/users/{id}","sarasa")).andExpect(status().isBadRequest());
    }

    @Test
    public void addUserShouldAddUser() {
        Mockito.doReturn(getUserForTest()).when(userService).addUser(any());
        User userAfterExecution = (User) userController.addUser(getUserForTest()).getBody();
        assertNotNull(userAfterExecution);
        assertEquals("Alberto",userAfterExecution.getFirstName());
        assertEquals("Gomez",userAfterExecution.getLastName());
        assertEquals("some@mail.com",userAfterExecution.getEmail());
        assertEquals(5,userAfterExecution.getId());
        assertEquals(Collections.emptyList(),userAfterExecution.getLoans());
    }

    @Test
    public void addUserShouldThrowExceptionIfJSONIsNotCorrect() throws Exception {
        mockMvc.perform(post("/users").contentType(MediaType.APPLICATION_JSON).content(
              "{\n" +
                      "\t\t\"id\" : \"SASRASA\",\n" +
                      "        \"first_name\": \"Martin\",\n" +
                      "        \"last_name\": \"Fabra\",\n" +
                      "        \"email\": \"somemail@mail.com\",\n" +
                      "        \"loans\": []\n" +
              "}")).andExpect(status().isBadRequest());
    }

    @Test
    public void deleteUserByIdShouldDeleteUserWithValidId() {
        Mockito.doNothing().when(userService).deleteUserById(5);
        ResponseEntity responseEntity = userController.deleteUserById(5);
        assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
    }

    @Test
    public void deleteUserShouldThrowExceptionIfIdIsNotNumber() throws Exception {
        mockMvc.perform(delete("/users/{id}","sarasa")).andExpect(status().isBadRequest());
    }


    private User getUserForTest() {
        User user = new User();
        user.setEmail("some@mail.com");
        user.setFirstName("Alberto");
        user.setLastName("Gomez");
        user.setId(5);
        user.setLoans(Collections.emptyList());
        return user;
    }
}